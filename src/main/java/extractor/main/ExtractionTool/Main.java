package extractor.main.ExtractionTool;



import java.io.IOException;

import java.net.URISyntaxException;

import java.time.Instant;
import java.util.*;

import extractor.main.FactorFinder.FCF;
import extractor.main.FactorFinder.FactorPrep;

import static extractor.main.FactorFinder.AuxiliaryMethods.*;



public class Main {



    public static void main( String[] args ) throws IOException, URISyntaxException {

        // Download and save the map data on which the mammals are located.
        MammalsMapExtractor mMapExtractor = new MammalsMapExtractor();
        MammalsExtractor mExtractor = new MammalsExtractor();
        System.out.println("Data from EMMA are available.");

        // Download and save the  data from WorldClim db.
        WorldClimExtractor worldClimExtractor = new WorldClimExtractor();
        worldClimExtractor.downLoadAll();
        System.out.println("Data from WorldClim are available.");

        // List of mammals
        ArrayList<Mammal> mammals = new ArrayList<>(mExtractor.getMammalsPresence());

        // List of Biovariables 7
        ArrayList<String> bioVariables = new ArrayList<>(worldClimExtractor.getDownloadedVariables());

        // Keys = id for location, double[] = coordinates
        HashMap<String, double[]> mammalsMap = mMapExtractor.getAreas();

        // Creation of boolean matrix
        MatrixCreator desMatrix = new MatrixCreator(mammals,mammalsMap,bioVariables);

        boolean[][] denseMatrix = desMatrix.getDenseMatrix();
        System.gc();

        System.out.println("Factors are being sought.");

        // GreConD implementation
        FCF conceptFinder = new FCF(transposeMatrix(denseMatrix));
        List<List<Integer>> listFactors = conceptFinder.getContext();

        System.out.println("Factor search complete.");

        System.out.println("Saving found factors.");

        // Saving found factors
        writeToFile(listFactors,"factorsRaw.txt");
        Instant finish = Instant.now();

        // Creation averages, differences and factorDetail files.
        FactorPrep fa = new FactorPrep("factorsRaw.txt");
        fa.transForm();

        System.out.println("Factors saved.");









    }
}
