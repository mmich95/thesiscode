package extractor.main.FactorFinder;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AuxiliaryMethods {

    private static String createReadAbleString(List<List<Integer>> listOfConcepts){


        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < listOfConcepts.size(); i+=2){
            stringBuilder.append("<").append(listOfConcepts.get(i) + ",")
                    .append(listOfConcepts.get(i+1)).append(">");

        }
        return stringBuilder.toString();
    }

    public static void writeToFile(List<List<Integer>> listOfConcepts,String fileName) throws URISyntaxException, IOException {

        String str = createReadAbleString(listOfConcepts);

        Path path = Paths.get(fileName);
        byte[] strToBytes = str.getBytes();

        Files.write(path, strToBytes);



    }

    public static boolean[][] transposeMatrix(boolean [][] m){
        boolean[][] temp = new boolean[m[0].length][m.length];
        for (int i = 0; i < m.length; i++)
            for (int j = 0; j < m[0].length; j++)
                temp[j][i] = m[i][j];
        return temp;
    }







}

