package extractor.main.FactorFinder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class FCF {

    private final boolean[][] matrix;


    private boolean[][] univesumMatrix;
    private List<List<Integer>> formalConcept = new ArrayList<>();
    private HashMap<Integer,ArrayList<Integer>> universumTable;


    public FCF(boolean[][] matrix){

        this.matrix = matrix;
        this.univesumMatrix = copyMatrix(matrix);
        this.universumTable = matrixToUniversum(matrix);
    }

    /**
     * Implementation of GreCond method
     * https://www.researchgate.net/publication/222544435_Discovery_of_optimal_factors_in_binary_data_via_a_novel_method_of_matrix_decomposition
     * @return
     */
    public List<List<Integer>> getContext(){
        int factorNumber = 0;
        while (!universumTable.isEmpty()){
            ArrayList<Integer> d = new ArrayList<>();
            int v = 0;

            while (existApproColumn2(d,v,univesumMatrix)){
                int j = chooseCollumn2(d,v,univesumMatrix);
                d.add(j);
                d = arrowUp(arrowDown(d));
                ArrayList<Integer> dDown = arrowDown(d);
                v = intersection(cartesian(dDown,d),univesumMatrix).size();
            }


            ArrayList<Integer> c = arrowDown(d);
            formalConcept.add(c);
            formalConcept.add(d);


            removeFromUniversum(cartesian(c,d));
            System.out.println("Factor " + factorNumber + " found.");
            factorNumber++;
        }
        return formalConcept;

    }

    private boolean existApproColumn2(ArrayList<Integer> d, int v,boolean[][] universum){
        for (int col = 0; col < univesumMatrix[0].length; col++){
            if(!d.contains(col) && crossFunction(d,col, universum).size() > v) {
                return true;
            }
        }
        return false;
    }


    private int chooseCollumn2(ArrayList <Integer> d, int v, boolean[][] universum){
        //long start = System.nanoTime();

        int size;
        int j = -1;
        int maxSize = v;

        for (int col = 0; col < univesumMatrix[0].length; col++){
            if (!d.contains(col)) {
                size = crossFunction(d,col,univesumMatrix).size();
                if(size > maxSize){
                    j = col;
                    maxSize = size;
                }
            }
        }




        return j;
    }

    private  ArrayList<int[]> crossFunction(ArrayList<Integer> d, int index,boolean[][] universum){


        ArrayList<Integer> indices = new ArrayList<>(d);
        indices.add(index);

        ArrayList<Integer> dDown = arrowDown(indices);
        int[][] cartesianProduct = cartesian(dDown,arrowUp(dDown));


        return intersection(cartesianProduct,universum);
    }


    private ArrayList<int[]> intersection(int[][] cartesianProduct, boolean[][] universum){
        ArrayList<int[]> intersection = new ArrayList<>();

        for (int[] ints : cartesianProduct) {
            if (universum[ints[0]][ints[1]]) {
                intersection.add(ints);
            }
        }
        return intersection;

    }


    private int[][] cartesian(ArrayList<Integer> extent, ArrayList<Integer> intent){
        int[][] cartesianProduct = new int[extent.size() * intent.size()][2];
        int k = 0;
        for (Integer integer : extent) {
            for (Integer value : intent) {
                cartesianProduct[k][0] = integer;
                cartesianProduct[k][1] = value;
                k++;
            }
        }


        return cartesianProduct;

    }

    private ArrayList<Integer> arrowDown(ArrayList<Integer> intentIndices){

        boolean isSharing;
        ArrayList<Integer> extentList = new ArrayList<>();

        // loop over all rows
        for(int i = 0; i < matrix.length; i++){
            isSharing = true;

            // loop over given colls
            for (Integer intentIndex : intentIndices) {
                if (!matrix[i][intentIndex]) {
                    isSharing = false;
                    break;
                }
            }
            // if object sharing all given attributes
            if(isSharing) {

                extentList.add(i);
            }
        }

        return extentList;
    }


    private ArrayList<Integer> arrowUp(ArrayList<Integer> extentIndices ){

        ArrayList<Integer> intentList = new ArrayList<>();
        boolean isSharing;

        // loop over all cols
        for(int i = 0; i < matrix[0].length; i++){

            isSharing = true;

            // loop over given rows
            for (Integer extentIndex : extentIndices) {
                if (!matrix[extentIndex][i]) {
                    isSharing = false;
                    break;
                }
            }

            if(isSharing) {
                intentList.add(i);
            }
        }

        return intentList;
    }


    public HashMap<Integer,ArrayList<Integer>> matrixToUniversum(boolean[][] matrix){
        HashMap<Integer,ArrayList<Integer>> universum = new LinkedHashMap<>();
        int rows = matrix.length;
        int cols = matrix[0].length;

        for(int i = 0 ; i < cols; i++){
            ArrayList<Integer> rowsIndices = new ArrayList<>();
            for(int j = 0; j < rows; j++){

                if(matrix[j][i]){
                    rowsIndices.add(j);
                }

            }
            if(!rowsIndices.isEmpty()){
                universum.put(i,rowsIndices);
            }

        }
        return universum;

    }


    private void removeFromUniversum(int[][] cartesianProduct){
        long start = System.nanoTime();

        for (int[] ints : cartesianProduct) {

            int row = ints[0];
            int col = ints[1];
            univesumMatrix[row][col] = false;

            if (universumTable.containsKey(col) && universumTable.get(col).contains(row)) {
                universumTable.get(col).remove((Integer) row);
                if (universumTable.get(col).isEmpty()) {
                    universumTable.remove(col);
                }
            } else if (universumTable.containsKey(col) && universumTable.get(col).isEmpty()) {
                universumTable.remove(col);

            }
        }
    }



    private boolean[][] copyMatrix (boolean[][] originalMatrix){
        boolean[][] result = new boolean[matrix.length][matrix[0].length];
        for (int i =0; i < matrix.length; i++){
            System.arraycopy(originalMatrix[i], 0, result[i], 0, matrix[0].length);
        }
        return result;
    }
}